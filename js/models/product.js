cc.models.HitCalculator = Backbone.Model.extend({
	get_hit_value : function(name, searched_name){
		var index = name.toLowerCase().indexOf(searched_name.toLowerCase()); 
		if ( index >= 0 && index < 4 ){ return 1; }
		if ( index >= 4 && index < 8 ){ return 2; }
		if ( index >= 8 && index < 12){ return 3; }
		if ( index >= 12 ){ return 4; }
		return 5;
	},
});

//Abstract
cc.models.PriceCalculator = Backbone.Model.extend({
	get_price : function(){}
});

cc.models.PriceCalculatorByGroup = Backbone.Model.extend({
	get_price : function(searched_name){
		return this.get('products').get_avg_by_group();
	},
});
cc.models.PriceCalculatorByHitLevel = Backbone.Model.extend({
	get_price : function(searched_name){
		return this.get('products').get_avg_by_hit_level(searched_name);
	},
});
cc.models.PriceCalculatorByAVG = Backbone.Model.extend({
	get_price : function(){
		return this.get('products').get_avg();
	},
});


cc.models.Product = Backbone.Model.extend({

	initialize : function(){
		this.hit_calculator = new cc.models.HitCalculator();
	},

	weight : {
		1 : 10,
		2 : 1,
		3 : 0.7,
		4 : 0.5,
		5 : 0.3
	},

	get_weight_by_hit_level : function(hit_level){
		return this.weight[hit_level];
	},

	get_price : function(){
		return this.get('price');
	},

	get_name : function(){
		return this.get('name');
	},

	get_thumbnail : function(){
		return this.get('thumbnail');
	},

	get_hit_level : function(searched_name){
		return this.hit_calculator.get_hit_value(this.get_name(),searched_name);
	},

	get_url : function(){
		return this.get('url');
	}

});