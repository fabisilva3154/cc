
//Controla que no muestre dos resultados por una sola busqueda
var mostrado = false;
cc.options.calculator = new cc.models.PriceCalculatorByGroup();

special_keywords = ['entrada'];
$(document).ready(function(){

	$(document).foundation();

	price_row_template = _.template($('#price-row').html());
	similar_product_template = _.template($('#similar-product').html());

	// MELI.init({client_id : 8870678822163041});
	$('#search-form').submit(function(e){
		e.preventDefault();
		$('#result').hide();
		$('#similars').empty();
		searchValue = $('#field').val().trim().toLowerCase();
		if ( searchValue.indexOf('un ') === 0 ){
			searchValue = searchValue.substr(3);
		}
		if ( searchValue.indexOf('una ') === 0){
			searchValue = searchValue.substr(4);
		}
		$('#search-form').css('margin-top', '10px');
		$('#spinner').show();
		if (includeStringInArray(searchValue, special_keywords)){
			specialSearch(searchValue);
		} else {
			searchML(searchValue, calculatePrice);
		}
		document.activeElement.blur();
	});
	$('#field').on('keydown',function(){
		mostrado = false;
	});
	$('#field').focus();
});

$(document).on('show-price', function(){
	$('#prices').hide();
	$('#thumbnail').hide();
});

function specialSearch(searchValue){
	if ( searchValue.indexOf('entrada') != -1 ){
		searchEntrada(searchValue, {
			success : function(entrada){
				showPrices(entrada.prices);
				showImage(entrada.thumbnail);
			},
			error   : function(){
				$('#cuanto-cuesta').text('Lo rompiste, boludo');
			}
		});
	}
}

function showPrices(prices){
	$('#prices-table tbody').html('')
	_.each(prices,function(price){
		$('#prices-table tbody').append(price_row_template({price : price}));
	});
	$('#prices').show();
}

function showImage(image_url){
	$('#thumbnail').show();
	$('#thumbnail').html('<img src="'+image_url+'">');
}

function searchEntrada(searchValue, opts){
	var options = _.extend({
		type : 'GET',
		url  : 'php_ajax/se.php',
		dataType : 'json',
		data : { event_name : $.trim(searchValue.replace('entrada','')) },
	}, opts);
	$.ajax(options);
}

//Busca el producto en ML
function searchML(searchValue, callback){
	var options = {
		type : 'GET',
		url : 'search',
		dataType : 'json',
		data : { searchValue : $.trim(searchValue) },
		success : function(response){
			var result = processMlResult(response.body.results);
			callback(result,searchValue);
		}
	};
	$.ajax(options);
}
//Procesa el resultado obtenido en ML
function processMlResult(result){
	cc.products = new cc.collections.Products();
	_.each(result, function(product){
		console.log(product)
		cc.products.add({price : product.price, name : product.title, thumbnail : product.thumbnail, url : product.permalink });
	});
	return cc.products;
}

//Redondea en multiplo de 5
function round(price){
	var resto = price % 5;
	return resto > 2.50 ? price + (5 - resto) : price - resto; 
}

//Esto obtiene por nivel de acierto en el termino que se busco y el encontrado
function getPrice(searchValue, results){
	cc.options.calculator.set({ products : cc.products});
 	return cc.options.calculator.get_price(searchValue);
}

//Calcula el precio a partir de los resultados obtenidos
function calculatePrice(results, searchValue){
	var price = getPrice(searchValue, results);
	var formatted_price = price > 50 ? round(price) : Number(price).toFixed(2);
	showCuantoCuesta(formatted_price);
	showProductsSimilarPrice(price, results);
}

//Muestra en pantalla el precio obtenido
function showCuantoCuesta(price){
	$('#result').show();
	$('#spinner').hide();
	if ( isNaN(price) ){
		$('#cuanto-cuesta').text('No se han encontrado resultados :(');
	} else {
		$('#cuanto-cuesta').text('$ '+price);
	}
	$(document).trigger('show-price');
}

function showProductsSimilarPrice(price, results){
	var similars = results.get_similar_price(price, 200);
	var product  = similars.shift();
	if ( !product ){ return ; }
	var interval = setInterval(function(){
		var product = similars.shift();
		if ( !product ){ 
			clearInterval(interval); 
			return; 
		}
		addSimilarPrice(product);
	}, 50);
}

function addSimilarPrice(product){
	var similar_html = similar_product_template({ product : product });
	$('#similars').append(similar_html);
}

function includeStringInArray(string, array){
	var includes = false;
	_.each(array, function(value){
		includes = includes || string.toLowerCase().indexOf((value.toLowerCase()+'')) != -1;
	});
	return includes;
}