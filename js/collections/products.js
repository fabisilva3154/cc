cc.collections.Products = Backbone.Collection.extend({

	model : cc.models.Product,
	
	//POR AVG
	get_price_sum : function(){
		var sum = 0;
		this.map(function(product){
			sum  += product.get_price();
		});
		return sum;
	},

	get_avg : function(){
		return this.get_price_sum() / this.length;
	},

	//POR NOMBRE DE LOS ARTICULOS ENCONTRADOS
	get_price_sum_by_hit_levels : function(searched_name){
		var sum = 0;
		this.map(function(product){
			sum += product.get_price() * product.get_weight_by_hit_level( product.get_hit_level(searched_name) ); 
		});
		return sum;
	},

	get_avg_by_hit_level : function(searched_name){
		var qty = 0;
		this.map(function(product){
			qty += product.get_weight_by_hit_level( product.get_hit_level(searched_name) );
		});
		return this.get_price_sum_by_hit_levels(searched_name) / qty;
	},

	//POR GRUPO
	get_price_group : function(){
		if ( !this.length ){
			return { 0 : new cc.models.Product({price : 0}) };
		}
		var groups = {};
		var firstPrice  = this.get_most_cheap().get_price();
		var actual_group_price = firstPrice;
		groups[actual_group_price] = [];

		var divisor_price = this.get_divisor_price();
		var sorted_products = this.get_sorted_by_price();
		_.each(sorted_products,function(product){
			if ( product.get_price() >= (divisor_price + actual_group_price) ){
				actual_group_price = product.get_price();
				groups[actual_group_price] = [];
			}
			groups[actual_group_price].push(product);
		});
		return groups;
	},

	get_divisor_price : function(products){
		return 600; //TODO
	},

	get_most_cheap : function(){
		return this.min(function(product){
			return product.get_price();
		});
	},

	get_sorted_by_price : function(){
		return this.sortBy(function(product){
			return product.get_price();
		});
	},

	get_avg_by_group : function(){
		var group = this.get_price_group();
		var max_group =  _.max(_.values(group),function(products){ 
			return products.length; 
		});
		var sum = 0; 
		_.each(max_group,function(product){
			sum += product.get_price();
		});
		return sum / max_group.length;
	},

	get_similar_price : function(price, range){
		range || ( range = 50 );
		var similars = [];
		this.each(function(product){
			if ( product.get_price() - range < price &&
				product.get_price() + range > price
			 ){
				similars.push(product);
			}
		});
		return _.sortBy(similars, function(product){ return Math.abs(product.get_price() - price); });
	},

});