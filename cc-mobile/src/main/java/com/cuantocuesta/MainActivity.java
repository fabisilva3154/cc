package com.cuantocuesta;

import android.app.Activity;
import android.os.Bundle;

// Dialog
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

// HTTP request
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends Activity {


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showPrice(View view)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Precio calculado");

        // Setting Dialog Message
        alertDialog.setMessage("$ 300");

        // Setting Icon to Dialog
        // alertDialog.setIcon(R.drawable.tick);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) { 
            // do nothing
            }
        });

        // Showing Alert Message
        alertDialog.show();
        
    }

}

