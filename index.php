<?php

require 'vendor/autoload.php';
require 'config/twig.php';

$router = new AltoRouter();
$router->setBasePath('/cc');

// index
$router->map( 'GET', '/', function() {
    global $twig;
    echo $twig->render('index.html');
});

//search
$router->map( 'GET', '/search', function(){
	require('src/Meli.php');
	$meli = new MeliSearch();
	$results = $meli->searchProduct($_GET['searchValue']);
	echo json_encode($results);
});




// match current request url
$match = $router->match();

// call closure or throw 404 status
if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] );
} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}

?>